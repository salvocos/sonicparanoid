## Important notice.

The version of SonicPatanoid in this repository is not maintained anymore.

The [latest version of SonicParanoid is hosted on GitLab](https://gitlab.com/salvo981/sonicparanoid2).  
You can find instructions on [SonicParanoid's web-page](http://iwasakilab.k.u-tokyo.ac.jp/sonicparanoid), 



Citation
===========

> Salvatore Cosentino and Wataru Iwasaki (2018) SonicParanoid: fast, accurate and easy orthology inference. Bioinformatics

> Volume 35, Issue 1, 1 January 2019, Pages 149–151,

> https://doi.org/10.1093/bioinformatics/bty631


### The information below are purely for reference

Changelog
===========

1.1.1 (January 24, 2019)
 - Enhancement: No restriction on file names
 - Enhancement: No restriction on symbols used in FASTA headers
 - Enhancement: Added file with genes that could not be inserted in any group (not orthologs)
 - Enhancement: Added some statistics on the predicted ortholog groups
 - Enhancement: Update runs are automatically detected
 - Enhancement: Improved inference of in-paralogs
 - Change: The directory structure has been redesigned to better support run updated

1.0.14 (October 19, 2018)
 - Enhancement: a warning is show if non-protein sequences are given in input
 - Enhancement: upgraded to MMseqs2 6-f5a1c
 - Enhancement: SonicParanoid is now available through Bioconda (https://bioconda.github.io/recipes/sonicparanoid/README.html)

1.0.13 (September 18, 2018)
 - Fix: allow FASTA headers containing the '@' symbol

1.0.12 (September 7, 2018)
 - Improved accuracy
 - Added new sensitivity mode (most-sensitive)
 - Fix: internal input directory is wiped at every new run
 - Fix: available disk space calculation

1.0.11 (August 7, 2018)
 - Added new program (sonicparanoid-extract) to process output multi-species clusters
 - Added the possibility to analyse only 2 proteomes
 - Added support for Python3.7
 - Python3 versions: 3.5, 3.6, 3.7
 - Upgraded MMseqs2 (commit: a856ce, August 6, 2018)

1.0.9 (May 10, 2018)
 - First public release
 - Python3 versions: 3.4, 3.5, 3.6
